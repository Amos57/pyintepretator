package com.amos.interpreter;

import static org.junit.Assert.assertTrue;

import com.amos.interpreter.parser.Line;
import com.amos.interpreter.parser.ParserUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.SplittableRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    static {
        Line.globalFunctions.put("print", new Function("print", Type.VOID, (list) -> {
            for (Value value : list) {
                System.out.print(value + " ");

            }
            System.out.println();
            return null;

        }));

        Line.globalFunctions.put("input", new Function("input", Type.STRING, (list) -> {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            try {
                Value value= new Value(Type.STRING,"");
                value.value= bufferedReader.readLine();
                return value;
            } catch (IOException e) {

            }
            return null;

        }));

        Line.globalFunctions.put("int", new Function("input", Type.INT, (list) -> {
            if (list.size() == 0) {
                throw new ParserException("need argument");
            }

            if (list.size() > 1) {
                throw new ParserException("need only one argument");
            }
            Value value = list.get(0);
            value.type=Type.INT;
            value.value=Integer.parseInt((String) value.value);

            return value;

        }));
    }
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()

    {
        assertTrue( true );
    }
    //@Test
    public void checkOperator(){
        String test1 = "546)+-+--+(";
        int offset1 = test1.length()-1;

        Object[] dataOperator1 = ParserUtils.getOperatot(test1,offset1);

        assertTrue((Boolean) dataOperator1[0]);

        assertTrue((char) dataOperator1[1]=='e');



        String test2 = "546/+-+-+(";
        int offset2 = test2.length()-1;

        Object[] dataOperator2 = ParserUtils.getOperatot(test2,offset2);

        assertTrue(!(Boolean) dataOperator2[0]);

        assertTrue((char) dataOperator2[1]=='/');


        String test3 = "546*-(";
        int offset3 = test3.length()-1;

        Object[] dataOperator3 = ParserUtils.getOperatot(test3,offset3);

        assertTrue((Boolean) dataOperator3[0]);

        assertTrue((char) dataOperator3[1]=='*');


        String test4 = "546%--(";
        int offset4 = test4.length()-1;

        Object[] dataOperator4 = ParserUtils.getOperatot(test4,offset4);

        assertTrue(!(Boolean) dataOperator4[0]);

        assertTrue((char) dataOperator4[1]=='%');


        String test5 = "546+(";
        int offset5 = test5.length()-1;

        Object[] dataOperator5 = ParserUtils.getOperatot(test5,offset5);

        assertTrue(!(Boolean) dataOperator5[0]);

        assertTrue((char) dataOperator5[1]=='e');



    }


    @Test
    public void finadFunctionName(){

        String fun1="fun";

        String names1 = ParserUtils.getFunctionName(fun1);

        assertTrue(names1.length()>0);

       // System.out.println(names1.get(0));
        assertTrue(names1.equals("function(33,44)"));


    }

    @Test
    public void finadParametersFunction(){

        String fun1="fun(33)";

        List<String> names1 = ParserUtils.getParameters(fun1);

        assertTrue(names1.size()>0);

       // System.out.println(names1.get(0));
        assertTrue(names1.get(0).equals("33"));


    }

    @Test
    public void testFineVariable(){
        String test1="var1 = var1 = int(input(\"wrfer\"))";
        assertTrue(ParserUtils.isVariable(test1));
    }

    @Test
    public void testCutVariable(){
        String test1=",i";
        assertTrue(ParserUtils.cutVar(test1,1).equals("i"));


        String test2=",i+h";
        assertTrue(ParserUtils.cutVar(test2,1).equals("i"));

        String test3="77, var_test +h";
        assertTrue(ParserUtils.cutVar(test3,4).equals("var_test"));
    }


    @Test
    public void testInvoke(){

        String line="varr = int( \"-343\")";
        try {
         ParserUtils.Argument argument= ParserUtils.invoke(line,7);
            assertTrue(argument.getValue().equals("-343.0"));
        } catch (ParserException e) {
            e.printStackTrace();
            assertTrue(3==9);
        }


    }


    @Test
    public void finadeIf(){
        String line="if (numberThree > i and 3==9):";
        String regular="(?<=if)(.*?)(?<=:)";


        Pattern pattern = Pattern.compile(regular);
        Matcher matcher= pattern.matcher(line);




        while (matcher.find())
        System.out.println(line.substring(matcher.start(),matcher.end()));
    }


}
