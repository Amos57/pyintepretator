package com.amos.interpreter;

import com.amos.interpreter.parser.Line;

import java.util.ArrayList;
import java.util.List;

public class Function {


    private String name;
    private Type typeReturn;

    public static String endString=null;

    List<Line> lines;
    Make make;

    public Function(String name, Type typeReturn,Make make) {
        this.name = name;
        this.typeReturn = typeReturn;
        lines= new ArrayList<>();
        this.make=make;

    }


    public boolean isEmpty(){
        return lines.isEmpty();
    }
    public void addLine(Line line){
        lines.add(line);
    }

    public List<Line> getLines() {
        return lines;
    }

    public Value make(List<Value> params) throws ParserException {
      if(make!=null)
            return   make.make(params);
    return null;
  }

public interface Make{

    Value make(List<Value> params) throws ParserException;
}
}
