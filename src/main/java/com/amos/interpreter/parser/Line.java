package com.amos.interpreter.parser;

import com.amos.interpreter.*;
import com.amos.interpreter.process.TypeProcess;

import java.util.*;

public class Line {

    public static Map<String,Value> globalVariabls = new HashMap<>();
    public static Map<String, Function> globalFunctions = new HashMap<>();

    private static final String EQUALS = "=";
    private static final String SPACE   = " ";
    private static final String parenthesis  = "(";

    private TypeProcess typeProcess;
    boolean isFunction;
    boolean isVariable;
    boolean isClass;




    private String[] name;
    private List<Value> params;
    private List<Character> operaters;

    private static int line;



    public Line(String data,int lineNum) throws ParserException {
         this.line=lineNum;
         params= new ArrayList();
         operaters= new ArrayList<>();
         parserLine(data);


    }

    public TypeProcess getTypeProcess() {
        return typeProcess;
    }

    public void parserLine(String data) throws ParserException {

        if (data.charAt(0) == '#' || (data.charAt(0) == '/' && data.charAt(1) == '/'))
            return;


        boolean isVariable = ParserUtils.isVariable(data);
        boolean isFunctionColl = ParserUtils.isFunctionColl(data);

        if (isFunctionColl) {
            String functionName = ParserUtils.getFunctionName(data);
            String rs = ParserUtils.removeBrackets(data);

            Function function = globalFunctions.get(functionName);
            if (function == null) {
                throw new ParserException("\'" + functionName + "\' is not defined");
            }
            ParserUtils.setNameContext(functionName);
            ParserUtils.setProcessType(TypeProcess.FUNCTION);
            List<Value> values = ParserUtils.parseParameters(rs);

            function.make(values);


        } else if (isVariable) {
            String variableName = ParserUtils.getVariableName(data);
            this.isVariable = isVariable;
            typeProcess = TypeProcess.VAR;
            ParserUtils.setNameContext(data);
            ParserUtils.setProcessType(TypeProcess.VAR);
            Value value = null;
            try {
                value = ParserUtils.parseParameters(data.substring(data.indexOf("=") + 1)).get(0);
            } catch (ParserException e) {
                e.printStackTrace();
            }
            globalVariabls.put(variableName, value);

         }else if(data.startsWith("for")){
             int end= data.indexOf(":");
             if(end==-1){
                 throw new ParserException("not found end ':'");
             }
             String params = data.substring(3,end).trim();

             String varName = ParserUtils.cutVar(params,0);
             String functionName = params.substring(params.indexOf("in")+2).trim();
             System.out.println("var: "+varName);
             System.out.println("functionName: "+functionName);


         }else{
            throw new ParserException("ivalid line - '"+data+"' in line - "+line );
        }




    }

    public boolean isFunction() {
        return isFunction;
    }

    public boolean isVariable() {
        return isVariable;
    }

    public boolean isClass() {
        return isClass;
    }

    public String[] getName() {
        return name;
    }

    public List<Value> getParams() {
        return params;
    }

    public int getLine() {
        return line;
    }


     String messageError(String messg){
        return String.format(App.TEMPLATE_ERROR_MASSAGE,line,messg);
    }

    private void readValue(String s) throws ParserException {
               char[] chars= s.toCharArray();
               StringBuilder param= new StringBuilder();
               for(int i=0;i<chars.length;i++){
                   switch (chars[i]){
                       case ' ':
                           break;
                       case 'T':
                           if(chars[i+1]=='r' && chars[i+2]=='u'&& chars[i+3]=='e'){

                               params.add(new Value(Type.BOOL,1));
                               i+=3;
                               break;
                           }
                       case 'F':
                           if(chars[i+1]=='a' && chars[i+2]=='l'&& chars[i+3]=='s' && chars[i+4]=='e'){
                               params.add(new Value(Type.BOOL,0));
                               i+=4;
                               break;
                           }
                       case '+':
                           operaters.add('+');
                           break;
                       case '*':
                           operaters.add('*');
                           break;
                       case '-':
                           operaters.add('-');
                           break;
                       case '/':
                           operaters.add('/');
                           break;
                       case '%':
                           operaters.add('%');
                           break;
                       case '\'':
                           StringBuilder stringBuilder = new StringBuilder();
                           i=writeStringFromJValue(s,i,stringBuilder);
                           params.add(new Value(Type.STRING,stringBuilder));
                           break;
                       case '\"':
                           StringBuilder sb = new StringBuilder();
                           i=writeStringFromJValue(s,i,sb);
                           params.add(new Value(Type.STRING,sb));
                           break;
                       default:
                           if(isNumber(chars[i]-'0')){
                               StringBuilder num = new StringBuilder();
                               i=writeNumberValue(s,i,num);
                               if(num.indexOf(".")!=-1)
                                    params.add(new Value(Type.FLOAT,Float.parseFloat(num.toString())));
                               else
                                   params.add(new Value(Type.INT,Integer.parseInt(num.toString())));

                           }else {
                               param.append(chars[i]);
                           }
                   }

               }
    }

    private void parser(String data) throws ParserException {

          String [] l= data.split("=");
          if(l.length>1){
              name=new String[l.length-1];
              isVariable =true;
              name[0]=l[0].trim();
              int e;
              if((e = name[0].indexOf(" "))!=-1){
                  throw new ParserException(messageError(name[0] +" contense \" \" in row "+ e));

              }
              for(int i=1;i<l.length-1;i++){

                  name[i]=l[i].trim();
                  int e1;
                  if((e1 = name[i].indexOf(" "))!=-1){
                      throw new ParserException(messageError(name[i] +" contense \" \" in row "+ e));

                  }
              }
              String val=l[l.length-1].trim();
              readValue(val);


          }

    }



    private int writeStringFromJValue(String value,int offset,StringBuilder sb) throws ParserException {

        Stack<Character> stack= new Stack<Character>();
        stack.push('\"');
        offset++;

        while (offset<value.length()){
            char c=value.charAt(offset);
            if(c=='\"')
                return offset++;



            sb.append(c);

            offset++;

        }

        throw new ParserException(messageError("error string \'\"\'"));

    }

    private int writeNumberValue(String value,int offset,StringBuilder sb)  {


        for(;offset<value.length();offset++){
            char t=value.charAt(offset);
            if(isNumber(t-'0') || t=='.'){
                sb.append(t);

            }else{
                return offset-1;
            }


        }
return value.length()-1;
        //throw new RuntimeException("invalid json");
    }

    public List<Character> getOperaters() {
        return operaters;
    }

    private int writeVariableValue(String value, int offset, StringBuilder sb)  {


        for(;offset<value.length();offset++){
            char t=value.charAt(offset);
            if(t==')'){

            }
            if(t==' ' ){
                return offset;
            }
            sb.append(value.charAt(offset));

        }

        throw new RuntimeException("invalid json");
    }

    char number(char c){
        int r=c-'0';
        if(isNumber(r)){

            return String.valueOf(r).charAt(0);
        }
        return '&';

    }
    boolean isNumber(int c){
        return c>-1 && c<10;
    }
}
