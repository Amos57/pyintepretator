package com.amos.interpreter.parser;

import com.amos.interpreter.*;
import com.amos.interpreter.process.TypeProcess;
import com.amos.logger.Logger;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserUtils {

    private static final ContextLine CONTEXT_LINE = new ContextLine();

//"\b[^()]+\((.*)\)$"
    public static final String FUNCTION_NAME= "^(\\w+(\\s+)?){2,}\\([^!@\\#$+%^]*\\)";

    //"([^,]+\(.+?\))|([^,]+)"
    public static final String PARAMETERS ="([^,]+\\(.+?\\))|([^,]+)";





    private static final Pattern FIND_FUNCTION_NAME =Pattern.compile(FUNCTION_NAME);

    private static final Pattern FIND_PARAMETERS_FUNCTION =Pattern.compile(PARAMETERS,
            Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);


    public static String removeBrackets(String data){

        int left=0,right=data.length()-1;
        boolean error=false;
        for (int i = 0; i < right; i++) {
            if(data.charAt(left)=='('){
                break;
            }
            left++;
        }
        for (int i = right; i>0 ; i--) {
            if(data.charAt(right)==')'){
                break;
            }
            right--;
        }
        Logger.assertError(left<right);
        return data.substring(left+1,right).trim();

    }

    public static List<Value> parseParameters(String data) throws ParserException {
        List<Value> list= new ArrayList<>();

        int cursor = 0;
        data=data.trim();


        while (cursor<data.length()){


            if(data.charAt(cursor)=='"'){
                String param = data.substring(cursor);

                int endParam=param.indexOf(",");
                Value val=null;
                if(endParam!=-1){
                    val=resFunctionTest(param.substring(0,endParam));

                }else
                    val=resFunctionTest(param);
                if (val==null)
                    throw new ParserException(data + ": not fount type:" + param);

                list.add(val);

                cursor+=endParam!=-1 ? endParam : param.length();



            }else if(data.substring(cursor).equals("True") || data.substring(cursor).equals("False") ) {
                String param = data.substring(cursor).trim();

                if (param.length() < 4)
                    throw new ParserException(data + ": not fount type:" + param);

                if (param.equals("True")) {
                    list.add(new Value(Type.BOOL, true));
                      return list;

                 }else  if( param.equals("False")) {
                     list.add(new Value(Type.BOOL, false));
                     return list;
                 }

                int endParam=param.indexOf(",");
                Value val=null;
                if(endParam!=-1){
                     val=resFunctionTest(param.substring(0,endParam));

                 }else
                    val=resFunctionTest(param);
                if (val==null)
                    throw new ParserException(data + ": not fount type:" + param);



                list.add(val);

            cursor+=endParam!=-1 ? endParam : param.length();
            }else if(data.charAt(cursor)>='0' && data.charAt(cursor)<='9'){
                String param = data.substring(cursor);
                int endParam=param.indexOf(",");
                Value number = null;
                if(endParam!=-1){
                     number=  resFunctionTest(param.substring(0,endParam));

                }else {
                     number =  resFunctionTest(param);
                }
                if (number==null)
                    throw new ParserException(data + ": not fount type:" + param);
                list.add(number);

                cursor+=param.length();//endParam!=-1 ? endParam : cursor+1;
            }else if(data.charAt(cursor)=='('){
                String param = data.substring(cursor);
                int endParam=param.indexOf(",");
                Value val=null;
                if(endParam!=-1){
                    val=  resFunctionTest(param.substring(0,endParam));

                }else
                    val=  resFunctionTest(param);

                if(val==null)
                    throw new ParserException("ivalid function: "+param);

               list.add(val);

                cursor=endParam!=-1 ? endParam : param.length();
            }else if(data.charAt(cursor)==' '){

                cursor++;
                continue;
            }else {

                String param=data.substring(cursor);
               // int endParam=param.indexOf(",");
                Value value=resFunctionTest(param);
               // }else {

              //      value=resFunctionTest(param.substring(0,endParam));
              //  }

                 if(value!=null)

                    list.add(value);
                cursor+= param.length();




            }


            while (cursor<data.length()-1 && data.charAt(cursor++)!=',' );

        }

        return list;

    }


    public static int findClosedCharacter(String txt, int st, char s, char closedSymbols){
        Stack<Character> stack= new Stack<>();
        stack.push(s);

        for (int i = st; i < txt.length(); i++) {
            if(txt.charAt(i)==s)
                stack.push(s);
            else if(txt.charAt(i)==closedSymbols)
                stack.pop();

            if(stack.size()==0)
                 return i;


        }
      return -1;
    }
/*
    public static Object resFunction(String data) throws ParserException {

        char array[]= data.toCharArray();

        ArrayList<Character> operators= new ArrayList<>();
        ArrayList<String>   arguments= new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            switch (array[i]){

                case '+':
                case '-':
                case '*':
                case '/':
                case '%':
                    operators.add(array[i]);
                    break;
                case 'T': {
                    Logger.assertError(i<=array.length-3);
                    String tr = new String(array, i, 4);
                    if (tr.equals("True"))
                        arguments.add("True");
                    else
                        throw new ParserException("invalid argument:" + tr);
                    break;
                }
                case 'F': {
                    Logger.assertError(i<=array.length-4);
                    String tr = new String(array, i, 5);
                    if (tr.equals("False"))
                        arguments.add("False");
                    else
                        throw new ParserException("invalid argument:" + tr);
                    break;
                }
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    String number=cutNumber(data.substring(i));
                    i+=number.length()-1;
                    arguments.add(number);
                    break;
                case '(':
                    int rightBorder=findClosedCharacter(data,i+1,'(',')');
                    arguments.add(String.valueOf(resFunction(data.substring(i+1,rightBorder))));
                    i=rightBorder;
                    break;

            }
        }


        if(arguments.size()==1){
                return getIntBool(arguments.get(0));
        }


        ArrayList<Character> operatorsPlusMinus= new ArrayList<>();
        ArrayList<Double> argumentsPlusMinus= new ArrayList<>();

        double res=   getBoolToNumber(arguments.get(0));
        argumentsPlusMinus.add(res);

        for (int i = 1,j=0; i < arguments.size() ;j++, i++) {

            String stringArg = arguments.get(i);
            Number arg2 = 0;
            if (stringArg.equals("True"))
                arg2 = 1;
            else if (stringArg.equals("False")) {
                arg2 = 0;
            } else
                arg2 =  Double.parseDouble(stringArg);

            char oper = operators.get(j);
            switch (oper) {
                case '+':
                case '-':
                    operatorsPlusMinus.add(oper);
                    argumentsPlusMinus.add((double) arg2);
                    res= (double) arg2;
                    break;

                case '*':
                    if (arg2 instanceof Double) {
                        double arg = (double) arg2;
                        res *= arg;

                    } else {
                        int arg = (int) arg2;
                        res *= arg;
                    }
                    argumentsPlusMinus.set(argumentsPlusMinus.size()-1,res);
                    break;
                case '/':
                    if (arg2 instanceof Double) {
                        double arg = (double) arg2;
                        res /= arg;
                    } else {
                        int arg = (int) arg2;
                        res /= arg;
                    }
                    argumentsPlusMinus.set(argumentsPlusMinus.size()-1,res);
                    break;
                case '%':
                    {
                    if (arg2 instanceof Double) {
                        double arg = (double) arg2;
                        res %= arg;
                    } else {
                        int arg = (int) arg2;
                        res %= arg;

                    }
                    argumentsPlusMinus.set(argumentsPlusMinus.size() - 1, res);
                }


            }

        }

        if(argumentsPlusMinus.size()==0 || argumentsPlusMinus.size()==1)
            return res;

        res=  argumentsPlusMinus.get(0);
        for (int i = 1,j=0; i < argumentsPlusMinus.size() ;j++, i++) {


            Double arg2=argumentsPlusMinus.get(i);

            char oper=operatorsPlusMinus.get(j);
            switch (oper) {
                case '+':
                    if (arg2 instanceof Double) {
                        double arg =   arg2;
                        res += arg;
                    } else {
                        int arg = arg2.intValue();
                        res += arg;
                    }
                    break;
                case '-':
                    if (arg2 instanceof Double) {
                        double arg = (double) arg2;
                        res -= arg;
                    } else {
                        int arg = arg2.intValue();
                        res -= arg;
                    }
                    break;
            }

        }
return res;
    }
*/

    private static String cutNumber(String num){
        StringBuilder res= new StringBuilder();

        for (int i = 0; i < num.length(); i++) {

            switch (num.charAt(i)){
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    res.append(num.charAt(i));
                    break;
                default:
                    return res.toString();


            }

        }

        return res.toString();

    }


    public static int findClosedCharacter(String txt, int st, char s){


        for (int i = st; i < txt.length(); i++) {
            char symbl= txt.charAt(i);

             if(symbl==s && txt.charAt(i-1)!='\\')
                 return i;

        }
        return -1;
    }

private static Value getDatavale(Argument data){

    if(data.equals("True"))
        return new Value(Type.BOOL,true);
    else if (data.equals("False"))
        return new Value(Type.BOOL,false);
    //if(data.value.contains("."))
    else if(data.type==Type.STRING)
          return new Value(Type.STRING,data.value);
    else
        return new Value(Type.FLOAT,Double.parseDouble(data.value));
    //else
      //  return data.isNefativ ? -Integer.valueOf(data.value):-Integer.valueOf(data.value);

}

    private static double getBoolToNumber(Argument data)  {


        if(data.equals("True"))
            return 1;
        else if (data.equals("False"))
            return 0;
        else return !data.isNefativ ? Double.parseDouble(data.value):-Double.parseDouble(data.value);


    }


    public static boolean isFunctionColl(String line){
        Pattern pattern= Pattern.compile("([a-zA-Z])+[0-9_]*(\\s)*([(])+([\\S\\D\\W])*[)][;]*");

        Matcher matcher=pattern.matcher(line);
        return matcher.matches();
    }

    public static String getFunctionName(String line){
        return line.substring(0,line.indexOf('(')).trim();
    }

    public static String getVariableName(String line){
        return line.substring(0,line.indexOf('=')).trim();
    }

    public static boolean isVariable(String line){
        Pattern pattern= Pattern.compile("([a-zA-Z])+[0-9_]*(\\s)*+[=](\\s)*([\\S\\D\\W])+");

        Matcher matcher=pattern.matcher(line);
        return matcher.matches();
    }


    public static List<String> getParameters(String line){

        Matcher matcher=FIND_PARAMETERS_FUNCTION.matcher(line);



        List<String> names = new ArrayList<>();
        while (matcher.find()) {
            names.add(line.substring(matcher.start(),matcher.end()));
            for (int i = 0; i <matcher.groupCount() ; i++) {
               // System.out.print(matcher.group(i));
            }


        }

        return names;
    }
//"([^,]+\(.+?\))|([^,]+)"


    public static Value resFunctionTest(String data) throws ParserException {

        char array[]= data.toCharArray();

        ArrayList<Argument> arguments= new ArrayList<>();
        Type type=Type.FLOAT;
        int index=0;
        for (int i = 0; i < array.length; i++) {
            switch (array[i]){


                case 'T': {
                    Logger.assertError(i<=array.length-3);
                    String tr = new String(array, i, 4);
                    if (tr.equals("True"))
                    {
                        Object[] dataOperator=getOperatot(data,i);
                        char operator= (char) dataOperator[1];
                        boolean isNegative= (boolean) dataOperator[0];
                        double val= 1;
                        if(isNegative){
                            val*=-1;
                            if(val>0)
                                isNegative=false;
                        }
                        i+=4;
                        arguments.add(new Argument(data,val,operator,isNegative,i,index++));
                    }
                    else
                        throw new ParserException("invalid argument:" + tr);
                    break;
                }
                case 'F': {
                    Logger.assertError(i<=array.length-4);
                    String tr = new String(array, i, 5);
                    if (tr.equals("False"))
                    {
                        Object[] dataOperator=getOperatot(data,i);
                        char operator= (char) dataOperator[1];
                        boolean isNegative= (boolean) dataOperator[0];
                        double val= 0;
                        if(isNegative){
                            val*=-1;
                            if(val>0)
                                isNegative=false;
                        }
                        i+=5;
                        arguments.add(new Argument(data,val,operator,isNegative,i,index++));
                    }
                    else
                        throw new ParserException("invalid argument:" + tr);
                    break;
                }
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    Argument argument=new Argument(data,i,index);

                    i+=argument.value.length()-1;
                    arguments.add(argument);
                    break;
                case '(':
                    {
                    int rightBorder = findClosedCharacter(data, i + 1, '(', ')');
                    if (rightBorder == -1)
                        throw new ParserException("Invalid mat functions:" + data);


                    Object[] dataOperator = getOperatot(data, i);
                    char operator = (char) dataOperator[1];
                    boolean isNegative = (boolean) dataOperator[0];
                    Value value =  resFunctionTest(data.substring(i + 1, rightBorder));
                    Double  val=((Number) value.value).doubleValue();
                    if (isNegative) {
                        val *= -1;
                        if (val > 0)
                            isNegative = false;
                    }
                    type=Type.FLOAT;
                    arguments.add(new Argument(data, val, operator, isNegative, i, index++));
                    i += data.length();
                    break;
                }
                case '-':
                case '+':
                case '/':
                case '*':
                case '%':
                case ' ':
                case ')':
                case ';':
                    break;
                case '\"':
                    type=Type.STRING;
                    int fi = findClosedCharacter(data,i+1,'"');
                    if(fi==-1)
                        throw new ParserException(data+": not fount \"");

                    //Logger.assertError (cursor<fi);
                    arguments.add(new Argument(data.substring(i+1,fi)));
                    i=fi+1;
                    break;
                default:

                    String varName=cutVar(data,i);



                    Value value= Line.globalVariabls.get(varName);




                    if(value==null&& CONTEXT_LINE.name.equals("print") && CONTEXT_LINE.typeProcess==TypeProcess.FUNCTION && varName.equals("end")){
                            i+=varName.length();
                            Object[] nextData=nextSymbol(data,i);
                            if((char)nextData[0]!='='){
                                throw new ParserException("wait '=' after 'end'");
                            }
                            i=(int)nextData[1];

                           Object[] findString=nextSymbol(data,i+1);
                           if((char)findString[0]!='"'){
                               throw new ParserException("value for 'end' not valid '" + data.substring(i+1).trim()+"'");
                           }
                           i=(int)findString[1];
                           int end = findClosedCharacter(data,i+1,'"');
                           if(end==-1)
                           throw new ParserException(data+": not fount \"");
                           String endValue=data.substring(i+1,end);
                           endValue=endValue.replace("\\n","\n").
                                             replace("\\t","\t").
                                             replace("\\r","\r");
                           Line.globalFunctions.get("print").endString=endValue;
                           i=end;
                           break;


                    }else if(value==null){

                        Function function=Line.globalFunctions.get(varName);
                        if(function==null)
                             throw new ParserException("\'" + varName + "\' is not defined");
                        String rs=ParserUtils.removeBrackets(data);

                        int lenghtData=varName.length()+2+rs.length();
                        //if (rightBorder == -1)
                           // throw new ParserException("Invalid mat functions:" + data);

                        List<Value> values=parseParameters(rs);

                         Value value1=function.make(values);
                          if(value1.type == Type.FLOAT  || value1.type == Type.INT){
                              Object[] dataOperator = getOperatot(data, i);
                              char operator = (char) dataOperator[1];
                              boolean isNegative = (boolean) dataOperator[0];
                              Double val=((Number)value1.value).doubleValue();
                              if (isNegative) {
                                  val *= -1;
                                  if (val > 0)
                                      isNegative = false;
                              }
                              type=Type.FLOAT;
                              arguments.add(new Argument(data, val, operator, isNegative, i, index++));


                          }else {
                              arguments.add(new Argument(value1.value.toString()));



                          }
                        i += lenghtData;


                         break;
                    }
                    if(value.type==Type.STRING){
                        type=Type.STRING;
                        arguments.add(new Argument(value.value.toString()));
                    }else {

                        Object[] dataOperator=getOperatot(data,i);
                        char operator= (char) dataOperator[1];
                        boolean isNegative= (boolean) dataOperator[0];
                        Object o = value.value;

                        Double doubl=((Number)o).doubleValue();


                        arguments.add(new Argument(data, doubl , operator, isNegative, i, index++));
                        type=Type.FLOAT;



                    }
                    i += varName.length();
                    //if(nextSymbol(data,i)!=',' || i!=data.length())
                      //  throw new ParserException("invalid character");

                    break;


            }
        }

        if(arguments.size()==0 && Function.endString==null){
            throw new ParserException("invalid data: ()");
        }else if(arguments.size()==0) {
            return null;
        }

        if(arguments.size()==1){
            return getDatavale(arguments.get(0));
        }

        if(type==Type.STRING){

            Argument firstArguments = arguments.get(0);
            for (int i = 1; i < arguments.size(); i++) {
                firstArguments=firstArguments.concat(arguments.get(i));
            }
            return new Value(type,firstArguments.value);

        }



        ArrayList<Argument> plusMinusArguments = new ArrayList<>();
        Argument firstArguments = arguments.get(0);
        plusMinusArguments.add(firstArguments);
        for (int i = 1; i <arguments.size() ; i++) {
            Argument currentArguments = arguments.get(i);

            switch (currentArguments.operator) {
                case '*':
                case '/':
                case '%':
                    plusMinusArguments.set(plusMinusArguments.size() - 1, firstArguments.doOperator(currentArguments));
                    firstArguments = plusMinusArguments.get(plusMinusArguments.size() - 1);
                    break;
                case 'e':
                    plusMinusArguments.add(currentArguments);
                    firstArguments = currentArguments;
                    break;
            }
        }


        if(plusMinusArguments.size()==1){
            return  new Value(Type.FLOAT,Double.parseDouble(plusMinusArguments.get(0).value));
        }

        Argument res=plusMinusArguments.get(0);
        for (int i = 1; i < plusMinusArguments.size(); i++) {
            res=res.doOperator(plusMinusArguments.get(i));
        }

        Double doubleValue=Double.parseDouble(res.value);
        int intValue=doubleValue.intValue();

        if(doubleValue-intValue==0) {
            return new Value(Type.INT,intValue);

        }
        return new Value(Type.FLOAT,doubleValue);
    }


    public static Object[] getOperatot(String data,int offset){
        boolean isNegative = false;
        if(offset==0)
            return new Object[]{isNegative,'e'};
        if(offset==1){
            isNegative=data.charAt(0)=='-';
            return new Object[]{isNegative,'e'};
        }
        offset--;
        char firfsOper=data.charAt(offset);
        if(firfsOper=='+' || firfsOper=='-')
        do{
            offset--;
            if(offset<0) {
                isNegative=firfsOper=='-';
                return new Object[]{isNegative,'e'};
            }

            char current = data.charAt(offset);

            if(current!='+' && current!='-'){
                break;
            }

            if(firfsOper=='+' && current=='+'){
                isNegative=false;
                firfsOper='+';
            }else if((firfsOper=='+' && current=='-') || (firfsOper=='-' && current=='+')){
                isNegative= true;
                firfsOper='-';
            }else if(firfsOper=='-' && current=='-'){
                isNegative= false;
                firfsOper='+';
            }


        }while (firfsOper=='+' || firfsOper=='-');
        isNegative=firfsOper=='-';

        switch (data.charAt(offset)) {
            case '*':
                firfsOper = '*';
                break;
            case '/':
                firfsOper = '/';
                break;
            case '%':
                firfsOper = '%';
                break;
            default:
                firfsOper='e';


        }

        return new Object[]{isNegative,firfsOper};

    }


   public static class Argument {
        private boolean isNefativ;
        private char operator;
        private int index;
        private String value;
        Type type;


       public Argument(String data){
           type=Type.STRING;
           this.value=data;
       }
        private Argument(){}

        Argument(String data, double value, char operator, boolean isNegative, int offset, int index){
            this(data,offset,index,String.valueOf(value));
            this.operator=operator;
            this.isNefativ=isNegative;

        }

       Argument(String data, int offset, int index){
           this(data,offset,index,null);
       }
       Argument(String data, int offset, int index, String value){


            if(value!=null){
                this.value=value;
                return;
            }
            Object[] dataOperator = getOperatot(data,offset);
            operator= (char) dataOperator[1];
            isNefativ= (boolean) dataOperator[0];


          this.value= cutNumber(data.substring(offset));
          if(isNefativ)
             this.value="-"+this.value;
       }

       @Override
       public String toString() {
           return "{\"operator\":\""+operator+"\",\"value\":\""+value+"\"}";
       }

       Argument concat(Argument arguments) throws ParserException {
           if(this.type!=Type.STRING || arguments.type!=Type.STRING)//да, мне пока лень
               throw new ParserException("string not concat to number:" +value+"+"+arguments.value);

           Argument newArguments= new Argument();
           newArguments.value=value+arguments.value;
           newArguments.type=Type.STRING;

           return newArguments;
       }

       public String getValue() {
           return value;
       }

       Argument doOperator(Argument arguments) throws ParserException {
           if(this.type==Type.STRING || arguments.type==Type.STRING)//да мне пока лень
               throw new ParserException("string not concat to number:" +value+"+"+arguments.value);


         Argument newArguments= new Argument();
            switch (arguments.operator){

                case 'e':
                        newArguments.value=String.valueOf(Double.valueOf(this.value)+Double.parseDouble(arguments.value));
                   break;
                case '*':
                    newArguments.value=String.valueOf(Double.valueOf(this.value)*Double.parseDouble(arguments.value));
                    break;
                case '/':
                    newArguments.value=String.valueOf(Double.valueOf(this.value)/Double.parseDouble(arguments.value));

                    break;
                case '%':
                    newArguments.value=String.valueOf(Double.valueOf(this.value)%Double.parseDouble(arguments.value));
                    break;

            }
            newArguments.isNefativ=newArguments.value.charAt(0)=='-';
            newArguments.operator= 'e';

            return newArguments;
     }


    }


    public static String cutVar(String data, int offset){
        int st = offset;
        char end=data.charAt(offset);
        if(st==data.length()-1)
            return String.valueOf(end);

        do {
            offset++;
            if(offset>data.length()-1){
                return data.substring(st);

            }

            end=data.charAt(offset);

        }while ((end>='A' && end<='Z') || (end>='a' && end<='z') || (end>='0' && end<='9') || end=='_');

        int ed=offset;
        String res=data.substring(st,ed);
       // System.out.println(res);
        return res;

    }


    public static Argument invoke(String line, int pos) throws ParserException {
        Type type;
        String varName = cutVar(line, pos);



        Function function = Line.globalFunctions.get(varName);
        if (function == null)
            throw new ParserException("\'" + varName + "\' is not defined");
        String params = line.substring(pos+varName.length());
        int rightBorder = findClosedCharacter(params, pos + 1, '(', ')');
        String rs = ParserUtils.removeBrackets(params);

        //if (rightBorder == -1)
         //   throw new ParserException("Invalid mat functions:" + line);

        List<Value> values = parseParameters(rs);
        Value value1 = function.make(values);
        if (value1.type == Type.INT) {
            Object[] dataOperator = getOperatot(line, pos);
            char operator = (char) dataOperator[1];
            boolean isNegative = (boolean) dataOperator[0];
            Integer val = ((Number) value1.value).intValue();
            if (isNegative) {
                val *= -1;
                if (val > 0)
                    isNegative = false;
            }

            return new Argument(line, val, operator, isNegative, pos, 0);


        }else if(value1.type == Type.FLOAT) {
            Object[] dataOperator = getOperatot(line, pos);
            char operator = (char) dataOperator[1];
            boolean isNegative = (boolean) dataOperator[0];
            Double val = ((Number) value1.value).doubleValue();
            if (isNegative) {
                val *= -1;
                if (val > 0)
                    isNegative = false;
            }

            return new Argument(line, val, operator, isNegative, pos, 0);


        }else if(value1.type == Type.STRING) {


            return new Argument(value1.value.toString());


        }
        return null;
    }

public static Value invoke(String line, int pos,String varName) throws ParserException {
    Function function = Line.globalFunctions.get(varName);
    if (function == null)
        throw new ParserException("\'" + varName + "\' is not defined");
    String params = line.substring(pos+varName.length());
    String rs = ParserUtils.removeBrackets(params);

    //if (rightBorder == -1)
    //   throw new ParserException("Invalid mat functions:" + line);

    List<Value> values = parseParameters(rs);

return function.make(values);
}

public static String getStringTamplate(String line){
        StringBuilder stringBuilder= new StringBuilder();

    for (int i = 0; i < line.length(); i++) {
        switch (line.charAt(i)){

            case '{':

                String block= line.substring(i+1);
                int endBlock = block.indexOf("}");
                if(endBlock==-1) {
                    stringBuilder.append("{");
                    break;
                }

                block=block.substring(0,endBlock);

                Value value= Line.globalVariabls.get(block);
                if(value!=null){
                    stringBuilder.append(value.value);
                    i=i+endBlock+1;
                }else
                    stringBuilder.append("{");
                break;
            case '"':
                if(i>0)
                   if(line.charAt(i-1)=='\\'){
                       stringBuilder.delete(stringBuilder.length()-1,stringBuilder.length());
                       stringBuilder.append("\"");
                   }
                break;
            default:
                stringBuilder.append(line.charAt(i));

        }
    }
        return stringBuilder.toString();
}


public static void setNameContext(String name){
        CONTEXT_LINE.name=name;
}

    public static void setProcessType(TypeProcess type){
        CONTEXT_LINE.typeProcess=type;
    }

//public static ContextLine getComtextLine(){

//}

    public static Object[] nextSymbol(String doc, int offset){
        char res=' ';
        while (offset<doc.length() && (res=doc.charAt(offset++))==' ');
        return offset==doc.length() ? new Object[]{' ',-1} : new Object[]{res,offset-1};
    }

}
