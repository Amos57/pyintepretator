package com.amos.interpreter;

import com.amos.interpreter.parser.Line;
import com.amos.interpreter.parser.ParserUtils;
import com.amos.interpreter.process.Process;
import com.amos.logger.Config;
import com.amos.logger.Level;


import java.io.*;



/**
 * Hello world!
 *
 */
public class App 
{

    static {

        Line.globalFunctions.put("print",new Function("print", Type.VOID, (list) -> {

            if(list.size()==0 && Function.endString==null){
                throw new ParserException("not fount parameters for 'print'");
            }else if(list.size()==0){
                System.out.println(Function.endString);
                Function.endString=null;
                return null;
            }

            Value last=list.get(list.size()-1);
           // if(list.size()>1)
            if(Function.endString!=null){
                last.value = last.value + Function.endString;
                last.type=Type.STRING;

            }
            for (Value value : list) {
                System.out.print(ParserUtils.getStringTamplate(value + " "));

            }
           if( Function.endString==null)
               System.out.println();
           else
               Function.endString=null;
            return null;

        }));

        Line.globalFunctions.put("input", new Function("input", Type.STRING, (list) -> {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            try {

                if(list.size()==1){
                    System.out.print(ParserUtils.getStringTamplate(list.get(0).value+""));
                }else if(list.size()>1){
                    throw new ParserException("function 'input' get one or no arguments");
                }

                Value value= new Value(Type.STRING,"");
                value.value= bufferedReader.readLine();
                return value;
            } catch (IOException e) {

            }
            return null;

        }));

        Line.globalFunctions.put("int", new Function("input", Type.INT, (list) -> {
            if (list.size() == 0) {
                throw new ParserException("need argument");
            }

            if (list.size() > 1) {
                throw new ParserException("need only one argument");
            }
            Value value = list.get(0);
            value.type=Type.INT;

            value.value=Integer.parseInt(value.value.toString());

            return value;

        }));

        Config config= Config.getConfig();
        config.setDoAssert(true);
        config.setLevel(Level.FILE);
        config.setFileName("logs/data.log");

    }

    public static final String TEMPLATE_ERROR_MASSAGE="Error line - %d: %s";


    public static void main( String[] args ) throws IOException {


        BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(new FileInputStream("project/run.py")));
        String line;
        int countLine=1;
        while ((line=bufferedReader.readLine())!=null) {
            try {

                if(line.equals(""))
                    continue;
                  Line line1= new Line(line,countLine++);
                  Process process= new Process(line1);
                  process.process();

            }catch (ParserException e){
                e.printStackTrace();
                System.exit(-1);
            }

        }



    }
}
