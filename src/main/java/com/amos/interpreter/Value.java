package com.amos.interpreter;

public class Value {

   public Type type;
   public Object value;

    public Value(Type type, Object value) {
        this.type = type;
        this.value = value;
        if(type==Type.FLOAT){

            double d=((Number)value).doubleValue();
            int i=((Number)value).intValue();

            if(d-i==0){
                this.value=i;
                this.type=Type.INT;
            }
        }
    }

    @Override
    public String toString() {
        if(type==Type.STRING)
            return (String) value;
        else if(type==Type.BOOL){
            if((Boolean)value){
                return "True";
            }else return "False";
        }else if(type==Type.INT)
            return String.valueOf(((Number) value).intValue());
        else if(type==Type.FLOAT)
            return String.valueOf((double)value);
        return super.toString();

    }
}
