package com.amos.interpreter;

public enum Type {

    BOOL, INT,FLOAT,OBJECT,STRING,VOID
}
